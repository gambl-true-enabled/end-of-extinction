package com.end.of.extinction

import android.content.Context
import android.webkit.JavascriptInterface

private const val END_TABLE = "com.END.table.123"
private const val END_ARGS = "com.END.value.456"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(END_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(END_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(END_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(END_ARGS, null)
}
